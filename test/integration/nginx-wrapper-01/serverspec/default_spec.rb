#
# Copyright (c) 2016-2017 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe package('nginx') do
  it { should be_installed }
end

describe process('nginx') do
  it { should be_running }
end

describe 'nginx' do
  it 'should listening on correct port' do
    expect(port(443)).to be_listening
  end
  it 'should serves a default html page' do
    curl = `HTTPS_PROXY='' https_proxy='' curl -sS -k https://localhost`
    expect(curl).to include('<title>Welcome to nginx!</title>')
  end
end
