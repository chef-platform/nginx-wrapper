{
  id: 'nginx-wrapper-01-centos-7.kitchen',
  cert: {
    encrypted_data:
    "4OCUawmaOw809+jBCxJRkGfznh18H5VeozsogsSyxJMh9AlpnUE62uYWklQn\n"\
    "DBf7ubm7dKhf5zM4u4S6kFLx9uyeZOYbYDIxLBPSl2HKSvQtg3+SslUmWwig\n"\
    "+ayuwTFAkgCo+/rXW52tAIkBGAfcCpWCmimPLgIK2r4kLU4yPn7OhdztzhZH\n"\
    "3uiSMra4WcOPQTIyJuswX8kaYuWBy6EhaCtNbYdBVBjjGCewzEnXmG4JuFPB\n"\
    "RfwOELiC2V12N80yXbHqFJyzrRcnJ6DOMs3as/yRFq7B0ueDvIxeo35OMJw0\n"\
    "Agbhta2fEPyYHaZQfsAl8CkZoV6PaIP/UJRL3JtBRvFv40q8eqeUYn/+fn6m\n"\
    "qqlndm/I7pKvcUVBqBL2n63LmdUnPePh7KCfXd+TNpkLmMCrMdDnyVMunu34\n"\
    "LK5dcHZSUgPIzhNpTtAPFwxhGg7KkMq4E+q/BMjCEvlQk8kA931tDrkjNhAY\n"\
    "sNERtW1psTy3EMH9guahTFNmDrJOW/hKxUKzNR8J59pgI6Wbf957tIfjXRyX\n"\
    "upv4GffxCLOmAGARZseUCbrCmwNmphmGLoK7VMYbOQupsRdIylXNV3X1qJlr\n"\
    "XT+0WJRj5Z7zpImn2Rj+Lvr5+q1YgVrGV2Bqi7n9Ggz10nVdAcW4XqneZzoh\n"\
    "jIFYduSCR6ks5bLNt4mOpW+3mI5xrfBh6W7hit7hOdrfpX1l9jNstf1dN4qA\n"\
    "ygAXCAVugyyU5ML50TN+Ed8qbMrczghL5WWiq6Upd1mV290tBkfsta4/pLJg\n"\
    "D018KyP5iPqK3IVdlbRAXKQkbe3NKcnaBP0wGrWsfDdxRUFZWRn0S+ZRlbVO\n"\
    "NBMtnADQZqcvtTeE7suAdCd6LCevfyIOD8NtFFviYSf+CmHf+NJ35fX3K83Z\n"\
    "6cogjWdwIb6CD2wi628BMQWw9KaXbK3D8u2d6fP8oSgedpG1Uu/HFl8WhUzT\n"\
    "i/tusBic92tpkTtJxNDouO8Ph6AICU4PZl99CnGd6mzwbCb2tKpXdJXvLhS1\n"\
    "f+D7CetG0A8lRTeTI2NTqoBOhz6iALYXdbKj3DAVCeN9QzCBUWg5pnoDCg2Z\n"\
    "nDedo1YvCgKrFNbW7y7AyWS417Lv+qnhoQpbhSBpZ7eLKpauYy/H4AuIh6tO\n"\
    "x/VGMgipPueu5B7Yr+9aqJmt11qLt1ATKyJm9YVgxp/zx4OMzTiPB44vdlk9\n"\
    "scCwKoV9Gl07AEzgzwX5bb77FDnr1UQ9hWbMWPzWt4cPZFpngY66I9mruUNG\n"\
    "hQJonVvwnrINmsuyw/xqs8TX9o3mryQZB/9L8Cg+uKCmQjTesLjDzBrTTK+l\n"\
    "z2r/Jg8KbPgvuPaaSYhm790aZrl/Ilef4keoTr3H5GQtSBTmZwZLta4EEd3s\n"\
    "g895EAWH4GUflVRZu4oiGOG483IsRGs9rCHtBg4DXn7I9H57iKI1qHOvJPLN\n"\
    "OhC1PWVfRdCgcAdi3BAu5Hy1smHbe+MeMOTwKjLw7aQ1L/EIDB56ta9upX+E\n"\
    "tVA/PuI11pjxyXPAaB5EOAMBeUqZawdCgwER3G9+aYPa/toG2QhujQj2HvPX\n"\
    "QMgWXePOqdZ9hpRaxkHYvJ17CapYkKHbYHpQ0HLUDe/envaMuQwB1yNtUmYM\n"\
    "4qmebIlfL+t+I2JQh+wppFU3YrVPVWDPZG8kHgJCf/UeYf1U1DRyu4fw1KVa\n"\
    "Plk93R/5nu8paiSnVL0GAewxEpG5nr3av7kViU/5+j56sogMX6krHcad0s2A\n"\
    "cV5ecNu055DQLskWUaRhPUW7h7BKOO5zuBjG7CN7g4jxrER8JzlklabkG9dY\n"\
    "augHBWDSYLT8dI0wV4ltGA8mGkfGkFUyjRa8dejV4Unu27jTzUDfLDRm+gCj\n"\
    "/nqm0rYBEMWXBnb03Q==\n",
    iv: "q9c1XLVq+e2nkKYnp1U2sA==\n",
    version: 1,
    cipher: 'aes-256-cbc'
  },
  key: {
    encrypted_data:
    "LPH/OoFZf9YGpPEXY9h226q73+PV1BYHbCQNyt1Jay01eRI2qyJJjWrDFjBJ\n"\
    "0ZrPi+WoyGId2JyWMme2JQ8wRsYsvWt7qRlMyRwh71wnFECQruQ/WvDgHF/T\n"\
    "iYETBsH0+6BDsQuQXF2G5KNpOACzHhwSEY6qqwyB24XwRX97wJ7XF8z/Zb7z\n"\
    "t0uc2UXBkKk0lNaMMvMnpTPkHF5u7EHeCCxDZFU+maDQ6tBj0R5jwrXyFuXA\n"\
    "eG5VZtsTdqrvf4/VxV1q/L7nA+iUFg3lju8I103OyzgCWCXyXwrKu64RZu3x\n"\
    "D7hphc+Ck+FpaC7ZJbYRB8YrX0ZIa2WZo01i9++9YrSSLgqSde731W+vzVRG\n"\
    "/j366wFhuXTQkfYqYasRIDRZVXpqsaKnhQIS26OgCtUCGttYt1JL43KGpMKM\n"\
    "5K6lXRrWIj9PvQDsCrsZlZPVUjcSKTlTNzhomDAzZiNKroVQ7+v7BHAMo5jh\n"\
    "6YGWo5nXbZ31BgIGJjsLpC+FvWzGOsOCtatFIp1RsKui9q2Q6Gt76q02lpXT\n"\
    "k3xE9AvZ/Ibi9dClD7it0WnMsAy6PxiIwIxGJQvWCYSAxa6fJYbO4SXUIRd8\n"\
    "b4oAAVu0s7POvTveKiyvGNBd66p7BF7Uz+hY4kogmon77gsWNgdCCpdxxSBi\n"\
    "lbqwz1TGz4E3svC2/Dp+f9y4I7Z1v29KHzDdxuxE5aIV1DITvx0j3hKHOX8X\n"\
    "LD6NR4WZ82JsLia/rJsJXe93JUvW+WyGODz6SaTiUB1Py5k0t69UuUz6KwNM\n"\
    "uuJFOCwbOAOzPA1dxJ123ryxz4fFKS/IzCvwfGXGaMrxpzQb7jiIGMAQw99T\n"\
    "H5Nx//6ASu1MDsdvKze+G45y2wjYtUUtMfSOZVPiFntPwhI3141cDPoOTbRj\n"\
    "orZIT/sUTtKEtXOfJPGwZPjNAZ5LSSNtC4f+SG+uTg+t4mo3hjxxIxTftHMM\n"\
    "cUlDjjm3kxid/q5m6zxwGglr7Jx0Et2kVKLLpPZ/XPZ153QbLONmFxS5sd3I\n"\
    "GJ/+GgnGUjKgYTanyzNjGKCe6RxrT9IHWZAYcjtsyfpTax7hIJf7uAEHA1sX\n"\
    "rjLkDPbcvwAhMPKwXKQs3jI0XmDZGaqSOqABR+qYqKGJg3Rw/CvrYx44eEQd\n"\
    "si9xlC3CshYJGLnhnRhzVsyXglrYItd674Sj2Mrrj3EzwBYEQ0km3BiuWpZR\n"\
    "bAy+1K3rs6GMO7MEHbk3HHRinIq5XEYx0ROHR6f9Vh0Dn7CihAIz7sq0bZC2\n"\
    "VZWfX4kXxosdBUpHqO2NEfZdDx4qOmW1ogjljw6pEGJGlJULyxQRwxFyCuG/\n"\
    "/SdvDFtGqAWjxRrzSWmNH3AtT7dh/bWq6617jmX23NOw5YC41Q+iqJ91hcdY\n"\
    "OHHlyIJkMa2m1+kQ8avJkgZG0hYfHm767inFiS5o84SHONv2Uoh737v/DLdh\n"\
    "2OcEu863d6nYBQizvbzzdE6raVI8O1WhlhNdEZLqOW0kmDXlF4Yolq3NDv4B\n"\
    "wwuOSwHL1FBFkFIrYsaY6exfgvWLLuUtN+HYO5jEx6eR1Oa1Wh/PiQ8MQjFC\n"\
    "K3Kp8vFeLiHD83Sb3X7jz03lvBphlh2V+eOrVjPrUs+w9ERe+WjVeQkgP9Sz\n"\
    "P3L3o/GPYDHECZgasz/yAygmCfcy8gQTb2HmdIXU/ayWxA1JRzx2OJW8IsT5\n"\
    "zoWhIj7DuYKtxalgHbWCEcTMdwSukjPp+U4bujjMJmLF7nLP6jldwDyaeI6b\n"\
    "nR8C0L1Joz8OKAzC9eO7XWKoXWHwdllfCR+8zVGccCbZvT1HgF4C79bxOoUM\n"\
    "mamJYpMuVsArCen6oBBtX1U2bAUFKOlAQMDtgGPj2gBnWK02LMrDFGzYsH1C\n"\
    "qV0CcZ3ZfNBg3aqdM4FXWi+4Dxxhw5s+NtLTCMpl/WrmtUan/nzn0gQsf5Wg\n"\
    "Ow97P1Rirr66Zrudawk4uS2l+QfzHhZp0HGkojowfqu7sCFsMyu6NdHkT8kf\n"\
    "bm9qrFL6GoX/54U685jPCwFMnBLk0Z+ULrlVZDWYfZznRGjqJyOCRUG1Sg4o\n"\
    "J5LfLItHC75FE2IrHFDXD9MksYwpBr8H9UJDgQyy5uokd28lcsplwUwuu0MA\n"\
    "CY0Ihir/63PxjA8ln78uFTEbTr3gWTbWJe6r7dUWAe1kvVDIgrYyKlmY/mTf\n"\
    "emvRFGQyWtlLRyc3AdDpPSfhi2SvpRYwAnU8qBWNDuaKzoFYkCznSrqElEJS\n"\
    "Oj0oHc6R7Ig9p7K77AI5X3OiQbo7uDpWs1qhMz5VS4F+xPLL/CzEl7Deqt6Z\n"\
    "Lm1Me1MudjpNwww6N8t5EuhkT5HTWV7wDgylSj1H99ekZRE9cePimx/eG8Hr\n"\
    "B7ApoPE=\n",
    iv: "Z8W45RotN5mBEODNTie9ZQ==\n",
    version: 1,
    cipher: 'aes-256-cbc'
  }
}
